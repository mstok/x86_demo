extern crate fatfs;

#[no_mangle]
pub extern fn create_fat_image(
    app_buf: *const u8, 
    app_size: u32, 
    image_buf: *mut u8, 
    image_size: u32) 
    -> bool 
{
    unsafe {
        image_buf[0] = app_buf[0];
        image_buf[1] = app_size as u8;
        image_buf[2] = image_size as u8;
    }

    return true;
}