# x86_demo

- Drop .COM, .EXE or a zip file,
- Enjoy the demo app.

### Used libraries
- v86
- Dropzone.js
- [zip.js](https://gildas-lormeau.github.io/zip.js/) - to unzip dropped file
- bootstrap
- jQuery

### Other
- FreeDOS
- SeaBIOS