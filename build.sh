#!/bin/sh

cd fatjs/

WABT=../../rust/wabt/bin
BINARYEN=../../rust/binaryen/build/bin
TARGET=wasm32-unknown-unknown
BINARY=target/$TARGET/release/fatjs.wasm
FINAL_BINARY=../assets/wasm/fatjs.wasm

cargo build --target $TARGET --release
$WABT/wasm-strip $BINARY
$BINARYEN/wasm-opt -o $FINAL_BINARY -Oz $BINARY
ls -lh $FINAL_BINARY
