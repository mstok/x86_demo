async function init() {
    const { instance } = await WebAssembly.instantiateStreaming(
        fetch("./assets/wasm/fatjs.wasm")
    );

    const answer = instance.exports.the_answer();
    console.log(answer);
}

init();

const output = document.getElementById('output');
const appSelector = document.getElementById('app-selector');

if (window.File && window.FileReader) {
    appSelector.addEventListener('change', event => {
    for (const file of event.target.files) {
        ProcessFile(file);
    }
  }); 
}

function ProcessFile(file) {
    //Hide input
    appSelector.style.display = 'none';

    if(file.type == "application/zip") {
        JSZip.loadAsync(file).then(function(zip) {
            //Clear Output
            output.innerHTML = '';

            zip.forEach(function (relativePath, zipEntry) {
                if(relativePath.toLowerCase().endsWith(".com") || relativePath.toLowerCase().endsWith(".exe")) {
                    let btn = document.createElement("button");
                    btn.innerHTML = zipEntry.name;
                    btn.onclick = function () {
                        zip.file(zipEntry.name).async("uint8array").then(content => {
                            CreateFloppy(content);
                        });
                    };
                    const li = document.createElement('li');
                    li.appendChild(btn);
                    output.appendChild(li);
                }
            });
        });
    }
    else {
        var reader = new FileReader();
        reader.addEventListener("load", function(event) {
            CreateFloppy(event.target.result);
        });
        reader.readAsBinaryString(file);
    }
}

function CreateFloppy (appContent) {
    //Clear Output
    output.innerHTML = '';
    //Print conten for now
    const li = document.createElement('li');
    li.textContent = appContent;
    output.appendChild(li);

    // Get the function out of the exports.
    const { sumArrayInt32, memory } = res.instance.exports
    
    // Create an array that can be passed to the WebAssembly instance.
    const array = new Int32Array(memory.buffer, 0, 5)
    array.set([3, 15, 18, 4, 2])
}

